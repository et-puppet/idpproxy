<?php

$config['metadata.sources'][] = array(
  'type'                    => 'pdo',
  'dsn'                     => 'mysql:host=' . $_SERVER['RDS_HOSTNAME']
                            .  ';dbname=' . $_SERVER['RDS_DB_NAME'],
  'username'                => $_SERVER['RDS_USERNAME'],
  'password'                => $_SERVER['RDS_PASSWORD'],
  'tablePrefix'             => 'sspmd_',
  'usePersistentConnection' => TRUE,
);

$config["enable.saml20-idp"] = true;
