<?php

$config = array(
  'admin' => array(
    'core:AdminPassword',
  ),

  'default-sp' => array(
    'saml:SP',
    'privatekey'  => 'idpproxy.key',
    'certificate' => 'idpproxy.pem',
    'entityID'    => $_SERVER['ENV_ENTITY_ID'],
    'idp'         => $_SERVER['ENV_IDP'],

    'authproc'  => array(
      20  => 'saml:NameIDAttribute',
    ),

  ),

);

