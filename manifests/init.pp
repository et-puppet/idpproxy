class idpproxy {

  file { '/etc/simplesamlphp/config.local.php':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}/config.local.php",
  }

  file { '/etc/simplesamlphp/metadata/saml20-idp-hosted.php':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}/saml20-idp-hosted.php",
  }

  file { '/etc/simplesamlphp/authsources.php':
    ensure  => file,
    owner   => 'root',
    group   => 'root',
    mode    => '0644',
    source  => "puppet:///modules/${module_name}/authsources.php",
  }

}

